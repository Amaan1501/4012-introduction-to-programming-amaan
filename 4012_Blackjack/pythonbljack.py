import random


def new_game():

    player1 = {
        "chip": {chips_input}, 
        "bet": {bet_placed},
        "card_input": {card_input}
    }
    return player1
    
    
    player2 = {
        "card2_input": {card2_input}
    }
    return player2



def init_actions():
     
    actions = ["draw", "stay"]
    return actions




if __name__ == "__main__":

    actions = init_actions()
    
    
    print ("Hello and welcome to Mannys blackjack! Pick an amount of chips to buy")
   
    while True:
        number = input("Player1 chips_input: ")
        try:
            chips_input = int(number)
            if chips_input < 1: 
                print("Enter a value greater than 0")
                continue
            break
        except ValueError:
            print("Please enter a valid number")     
    
    print(f'you have cashed {chips_input}')
    

    play = True
    while play:

        
        while True:
            number = input("Place a valid bet:")
            try:
                bet_placed = int(number)
                if bet_placed > chips_input:  
                    print("You dont have more chips") 
                    continue
                elif bet_placed < 1:  
                    print("The chips you have entered are low")
                    continue
                break
            except ValueError:
                print("Enter a valid number")     
            
        print(f'You have {bet_placed}for this game')


        print ("Dealer: 'shuffles 2 cards each to player1'")

       
        a = random.randint(1,11)
        b = random.randint(1,11)
        card_input = a + b
        print (f'You have:{card_input}')

       
        player1 = new_game()       



       
        if card_input == 21: 
            print("Player1 has won!")           
           
            chips_input += bet_placed
            
            print(f'You have {chips_input} chips')

        elif card_input == 22: 
            print("player2 has won!")
            chips_input -= bet_placed
            print(f'You lost your betting chips from your pile')
            print(f'Your total chip pile {chips_input}')
                    
        
        print ("Dealer: 'Shuffles and hands out 2 cards each to player2'")

        
        x = random.randint(1,11)
        y = random.randint(1,11)
        card2_input = x + y
        print (f'player2 has:{card2_input}')
    
        player2 = new_game()
       
        if card2_input > 21: 
            print("Player1 wins")
            bet_placed += 5
            chips_input += bet_placed
            print(f'+5 chips are added to your pile')
            print(f'Your {chips_input} chips')

        elif card2_input == 21: 
            print("player2 has won!")
            chips_input -= bet_placed
            print(f'Your total chip pile {chips_input}')

        elif card2_input < 17: 
            print("player2 takes a card from the pile")
            c2 = random.randint(1,11)
            card2_input += c2
            print (f'player2 has {card2_input}')

            if card2_input > 21: 
                print("player1 has won")
                bet_placed += 5
                chips_input += bet_placed
                print(f'Your total chip pile {chips_input}')

            elif card_input == 21: 
                print("Player2 has won")
                chips_input -= bet_placed
                print(f'you have {chips_input} chips')

            else: 
                print ("You have drawn!")

# """---------------------------------------------------------------------------------------------------------------------------"""
             
        
        answer = input("Would you like to bust or stay?: ")
        if answer == "bust":
            print(f'You have this many {chips_input} chips')
            play = False 