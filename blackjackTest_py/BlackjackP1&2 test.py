# Testing win, loss and draw:
# - win
# - lose
# - draw
# - player2 win
# - player2 lose


"""-----------------------------------------------------"""

# 


# If player1 has 22 cards= loss
def player_lose_case(n):
    return n == 22
    return False

def test_player_lose_case():
    assert player_lose_case(22)


#If player1 has 21 they win
def player_win_case(n):
    return n == 21
    return False

def test_player_win_case():
    assert player_win_case(21)




# If player2 has 21 they win
def computer_win_case(n):
    return n == 21
    return False

def test_computer_win_case():
    assert computer_win_case(21)




# OPtion of drawing 
def player_option_case(n):
    return n < 22
    return False

def test_player_option_case():
    assert player_option_case(18)





# If player2 has  less than 21 the player1 wins
def computer_lose_case(n):
    return n > 21
    return False

def test_computer_lose_case():
    assert computer_lose_case(22)


# T#if player 2 has more than 17 player 1 draws a card
def computer_draw_card_case(n):
    return n < 17
    return False

def test_computer_draw_card_case():
    assert computer_draw_card_case(12)

"""-----------------------------------------------------"""

# Both players have under 21

def card_total_player(n):
    return n < 21
    return False
def card_total_computer(n):
    return n < 21
    return False

def test_card_total():
    assert card_total_player(19)
    assert card_total_computer(20)


# Both players have same cards
def card_total_equal_player(n):
    return n == 21
    return False
def card_total_equal_computer(n):
    return n == 21
    return False

def test_card_total_equal():
    assert card_total_equal_player(21)
    assert card_total_equal_computer(21)


